FROM docker.io/python:3.11

# Install Git LFS
RUN apt-get update && \
    apt-get install -y git-lfs && \
    git lfs install

    
WORKDIR /app

RUN adduser --system --uid 999 --group --home /app app

COPY requirements.txt .

RUN pip install --no-cache-dir -r requirements.txt

COPY --chown=app:app . /app

USER app

EXPOSE 8080

ENV PYTHONUNBUFFERED=1

CMD ["gunicorn", "-w 2", "-b 0.0.0.0:8080", "core.wsgi", "--log-config", "gunicorn-logging.conf", "--timeout", "600"]
